import cv2
import matplotlib.pyplot as plt
import numpy as np

########
# Hole #
########

def find_hole(image: np.ndarray):
    # convert to RGB
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # convert to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

    # create a binary thresholded image
    _, binary = cv2.threshold(gray, 225, 255, cv2.THRESH_BINARY_INV)
    # show it
    # plt.imshow(binary, cmap="gray")
    # plt.show()

    # print(binary[100][100], binary[0][0])

    # array of y pixels num
    nonzero_pixel_num_y = np.sum(binary/255, axis=1)

    # plt.plot(nonzero_pixel_num_y)
    # plt.show()

    # detecting hole y range
    threshold = max(nonzero_pixel_num_y) / 2.7

    hole_y = nonzero_pixel_num_y <= threshold

    # plt.plot(hole_y)
    # plt.show()

    platform_start_y = np.where(hole_y == 0)[0][0]
    if(platform_start_y == -1):
        raise RuntimeError('Error while trying to find a hole')

    hole_start_y = np.where(hole_y[platform_start_y:] == 1)[0][0]
    if(hole_start_y == -1):
        raise RuntimeError('Error while trying to find a hole')
    hole_start_y += platform_start_y

    hole_end_y = np.where(hole_y[hole_start_y:] == 0)[0][0]
    if(hole_end_y == -1):
        raise RuntimeError('Error while trying to find a hole')
    hole_end_y += hole_start_y - 1

    train_start_y = hole_end_y + 1

    return hole_start_y, hole_end_y


def check_hole(cropped):
    gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
    bin_hole = cv2.inRange(gray, 0, 230)
    return cv2.countNonZero(bin_hole) < 1000

def check_center(image, avg):
    hsv = cv2.cvtColor(image[avg - 70:avg + 70], cv2.COLOR_BGR2HSV)
    range = cv2.inRange(hsv, (0, 240, 240), (100, 255, 255))
    return cv2.countNonZero(range) < 7000

