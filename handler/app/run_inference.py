import base64
import json
import urllib
import os

AIRFLOW_USERNAME = os.environ["AIRFLOW_USERNAME"]
AIRFLOW_PASSWORD = os.environ["AIRFLOW_PASSWORD"]

AIRFLOW_HOST = os.environ["AIRFLOW_HOST"]
AIRFLOW_PORT = os.environ["AIRFLOW_PORT"]
DAG_ID = os.environ["DAG_ID"]


def trigger_airflow(resource_id):
    auth_b64_code = base64.b64encode(f"{AIRFLOW_USERNAME}:{AIRFLOW_PASSWORD}".encode("utf-8")).decode()

    data = {
        "conf": {"resource_id": resource_id}
    }

    json_data = json.dumps(data)
    json_as_bytes = json_data.encode('utf-8')

    req = urllib.request.Request(f'http://{AIRFLOW_HOST}:{AIRFLOW_PORT}/api/v1/dags/{DAG_ID}/dagRuns')
    req.add_header("Content-type", "application/json")
    req.add_header("Authorization", "Basic %s" % auth_b64_code)
    req.add_header('Content-Length', len(json_as_bytes))
    req.add_header("Accept", "application/json")

    resp = urllib.request.urlopen(req, json_as_bytes)
    print("RESPONSE: ", json.load(resp))
