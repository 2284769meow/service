import os
from flask import Flask, flash, request, redirect, url_for
from run_inference import trigger_airflow
import uuid
import json
from flask_cors import CORS, cross_origin

WEB_PORT = os.environ["WEB_PORT"]

ALLOWED_EXTENSIONS = {'pcd'}

app = Flask(__name__)
app.config['PAYLOADS_PATH'] = os.environ["PAYLOADS_PATH"]

cors = CORS(app)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@cross_origin()
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return json.dumps({"ok": False, "error": "Invalid request"})
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return json.dumps({"ok": False, "error": "Nothing to upload"})

        if file and allowed_file(file.filename):
            filename = "pointcloud.pcd"
            payload_id = str(uuid.uuid4())
            payload_dir_path = os.path.join(app.config['PAYLOADS_PATH'], "tmp", payload_id)
            os.mkdir(payload_dir_path)
            file.save(os.path.join(payload_dir_path, filename))
            trigger_airflow(payload_id)
            return json.dumps({"ok": True, "id": payload_id})
    else:
        return json.dumps({"ok": False, "error": "Invalid request"})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int(WEB_PORT))
