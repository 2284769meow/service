from typing import *

import cv2
import numpy as np
import math

from math import atan2, degrees


def AngleBtw2Points(a: list, b: list) -> float:
    changeInX = b[0] - a[0]
    changeInY = b[1] - a[1]
    return degrees(atan2(changeInY, changeInX))  # remove degrees if you want your answer in radians


def rotate_image(image: np.ndarray, angle: float) -> np.ndarray:
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR, borderValue=(255,255,255))
    return result


def ransac(points: np.ndarray,
           min_inliers: int = 4,
           max_distance: float = 3,
           outliers_fraction: float = 0.5,
           probability_of_success: float = 0.99999) -> np.ndarray:
    """
    RANdom SAmple Consensus метод нахождения наилучшей
    аппроксимирующей прямой.

    :param points: Входной массив точек формы [N, 2]
    :param min_inliers: Минимальное количество не-выбросов
    :param max_distance: максимальное расстояние до поддерживающей прямой,
                         чтобы точка считалась не-выбросом
    :param outliers_fraction: Ожидаемая доля выбросов
    :param probability_of_success: желаемая вероятность, что поддерживающая
                                   прямая не основана на точке-выбросе
    :param axis: Набор осей, на которых рисовать график
    :return: Numpy массив формы [N, 2] точек на прямой,
             None, если ответ не найден.
    """

    # Давайте вычислим необходимое количество итераций
    num_trials = int(math.log(1 - probability_of_success) /
                     math.log(1 - outliers_fraction ** 2))

    best_num_inliers = 0
    best_support = None
    for _ in range(num_trials):
        # В каждой итерации случайным образом выбираем две точки
        # из входного массива и называем их "суппорт"
        random_indices = np.random.choice(
            np.arange(0, len(points)), size=(2,), replace=False)
        assert random_indices[0] != random_indices[1]
        support = np.take(points, random_indices, axis=0)

        # Здесь мы считаем расстояния от всех точек до прямой
        # заданной суппортом. Для расчета расстояний от точки до
        # прямой подходит функция векторного произведения.
        # Особенность np.cross в том, что функция возвращает только
        # z координату векторного произведения, а она-то нам и нужна.
        cross_prod = np.cross(support[1, :] - support[0, :],
                              support[1, :] - points)
        support_length = np.linalg.norm(support[1, :] - support[0, :])
        # cross_prod содержит знаковое расстояние, поэтому нам нужно
        # взять модуль значений.
        distances = np.abs(cross_prod) / support_length

        # Не-выбросы - это все точки, которые ближе, чем max_distance
        # к нашей прямой-кандидату.
        num_inliers = np.sum(distances < max_distance)
        # Здесь мы обновляем лучший найденный суппорт
        if num_inliers >= min_inliers and num_inliers > best_num_inliers:
            best_num_inliers = num_inliers
            best_support = support

    # Если мы успешно нашли хотя бы один суппорт,
    # удовлетворяющий всем требованиям
    if best_support is None:
        raise Exception('Line not found')

    # Спроецируем точки из входного массива на найденную прямую
    support_start = best_support[0]
    support_vec = best_support[1] - best_support[0]
    # Для расчета проекций отлично подходит функция
    # скалярного произведения.
    offsets = np.dot(support_vec, (points - support_start).T)
    proj_vectors = np.outer(support_vec, offsets).T
    support_sq_len = np.inner(support_vec, support_vec)
    projected_vectors = proj_vectors / support_sq_len
    projected_points = support_start + projected_vectors

    return projected_points
