from typing import *

import cv2
import numpy as np
import math

from opencv_.helpers import *


def crop(im: np.ndarray) -> np.ndarray:
    """
    Cropping white space of a picture

    :param im: OpenCV image object
    :return: Cropped OpenCV image
    """
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    x, y, w, h = cv2.boundingRect(cv2.inRange(gray, 0, 220))
    return im[y:y + h, x:x + w]


def rotate(im: np.ndarray) -> (np.ndarray, int):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    fff = cv2.inRange(gray, 220, 255)
    fff = cv2.erode(fff, np.ones((7,7),np.uint8), iterations=1)
    pts = []
    for x in range(fff.shape[1]):
        y = fff[:, x].argmax()
        pts.append((x, y))
        # cv2.circle(im, (x, y), 5, (0, 0, 255), -1)

    ran = ransac(np.array(pts)).astype(int)
    ang = AngleBtw2Points(ran[0], ran[-1])
    # cv2.line(im, tuple(ran[0].tolist()), tuple(ran[-1].tolist()), (0, 0, 255), 2)
    # for p in ran:
    #     cv2.circle(im, (int(p[0]), int(p[1])), 2, (0, 0, 255), -1)
    return rotate_image(im, ang), min(ran[:,1])


def proem(im: np.ndarray):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    bin = cv2.inRange(gray, 0, 220)
    bin = cv2.erode(bin, np.ones((5,5),np.uint8), iterations=2)
    bin = 255 - bin
    # cv2.imshow('erode', bin)
    wrap = cv2.cvtColor(bin, cv2.COLOR_GRAY2BGR)
    pts = []
    for x in range(bin.shape[1]):
        t = bin[:, x].argmax()
        y = t+bin[t:, x].argmin()
        pts.append((x, y))
        # cv2.circle(wrap, (x, y), 5, (0, 0, 255), -1)
    ran = ransac(np.array(pts)).astype(int)
    sums = []
    for i in range(5, 26, 5):
        sum = 0
        for p in ran:
            try:
                sum += bin[p[1]-i, p[0]]
            except:
                pass
            cv2.circle(wrap, (p[0], p[1]-i), 1, (0, 0, 255), -1)

        sums.append(sum)
    sums = np.array(sums)
    sums -= sums.max()
    # print(sums[0])
    # cv2.imshow('wrap', wrap)
    return sums[0]