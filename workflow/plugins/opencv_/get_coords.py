from typing import *
import cv2
import numpy as np
import os
import json



def get_info_from_file(fn: Union[str, bytes, os.PathLike]) -> (float, float, float, float, float):
    with open(fn, 'r') as f:
        minx, miny, minz, maxz = map(float, f.read().split())
    mz = 179 / (maxz - minz)
    return minx, miny, minz, maxz, mz


def get_coords(object, im: np.ndarray, box: ((int, int), (int, int)), minx, miny, minz, maxz, mz):
    rrr = im[box[0][1]:box[1][1], box[0][0]:box[1][0]]

    roi = cv2.cvtColor(rrr, cv2.COLOR_BGR2HSV)
    roi_reshaped = np.reshape(roi, (roi.shape[0] * roi.shape[1], 3))

    # ran = cv2.inRange(roi, (25, 220, 220), (27, 255, 255))

    cropped_pixels = roi_reshaped[(roi_reshaped[:, 1] > 220) & (roi_reshaped[:, 2] > 220)]
    values, counts = np.unique(cropped_pixels[:, 0], return_counts=True)
    values = values[counts > cropped_pixels.shape[0] // 100]
    # counts = counts[counts > 70]

    np_box = np.array(box)
    np_box = np_box / 200
    np_box[:, 0] += minx
    np_box[:, 1] += miny

    if object == 'human' or object == 'Adult':
        second = maxz - 1
    else:
        second = (values.min() / mz) + minz + 1

    return np.append(np_box, [[(values.min() / mz) + minz], [second]], axis=1)


def transform_coords(coords: np.ndarray) -> (np.ndarray, np.ndarray, np.ndarray):
    position = coords.mean(axis=0)
    position[1] *= -1

    rotation = np.array([0.0, 0.0, np.pi / 2])

    dimensions = coords[1] - coords[0]

    return position, rotation, dimensions


def generate_figure(object: str,
                    position: Union[np.ndarray, list],
                    rotation: Union[np.ndarray, list],
                    dimensions: Union[np.ndarray, list]) -> dict:
    return {
        "object": object,
        "geometry": {
            "position": {
                "x": position[0],
                "y": position[1],
                "z": position[2]
            },
            "rotation": {
                "x": rotation[0],
                "y": rotation[1],
                "z": rotation[2]
            },
            "dimensions": {
                "x": dimensions[0],
                "y": dimensions[1],
                "z": dimensions[2]
            }
        }
    }


def generate_json(figures: List[dict], door: str) -> str:
    return json.dumps({
        "figures": figures,
        "door": door
    }, indent=4)
