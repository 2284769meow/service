import os
import inspect

from utils.context import get_resource_id

PAYLOADS_PATH = "/tmp"


def get_dag_payload_path(context=None):
    res_id = get_resource_id(context)
    dag_payload_path = os.path.join(PAYLOADS_PATH, res_id)

    return dag_payload_path


def get_task_payloads_path(context=None):
    dag_payload_path = get_dag_payload_path(context)
    task_id = inspect.stack()[1].function

    task_payload_path = os.path.join(dag_payload_path, task_id)
    try:
        os.makedirs(task_payload_path)
    except FileExistsError:
        print(f"{task_payload_path} - already exists")

    return task_payload_path


def get_unique_dag_id(dag_module):
    abs_path = os.path.abspath(dag_module)
    full_path = os.path.join(abs_path, dag_module)
    dag_id = '_'.join(full_path.split("/")[4:]).split(".")[0]

    return dag_id


def get_current_dir_path(dag_module):
    return "/" + os.path.join(*os.path.abspath(dag_module).split("/")[:-1])
