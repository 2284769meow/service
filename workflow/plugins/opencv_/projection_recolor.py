from typing import Union
import os

from matplotlib import pyplot as plt
from sklearn.neighbors import KDTree
import numpy as np
import cv2


def read_pcd(fn: Union[str, bytes, os.PathLike]) -> (str, np.ndarray):
    """
    Read pcd file

    Args:
        fn: Path to the file

    Returns: Tuple of header string and points numpy array
    """

    points = []
    header = ''
    with open(fn, 'r') as inp:
        for i, line in enumerate(inp):
            if i <= 10:
                header += line
            else:
                points.append(list(map(float, line.split()[:3])))

    points = np.array(points)
    return header, points


def delete_outlines(points: np.ndarray, leaf_size: int = 40, k: int = 10, offset: float = 0.3) -> np.ndarray:
    kdt = KDTree(points, leaf_size=leaf_size)
    dist, neig = kdt.query(points, k=k)
    return points[dist.sum(axis=1) < offset]


def create_opencv_image(pts: np.ndarray) -> (np.ndarray, float, float, float, float):
    minn, maxx = pts.min(axis=0), pts.max(axis=0)

    minx_f, miny_f = minn[0], minn[1]

    pts[:, 0] -= minn[0]
    pts[:, 1] -= minn[1]
    pts[:, :2] *= 200

    while True:
        minn, maxx = pts.min(axis=0), pts.max(axis=0)

        minz, maxz = minn[2], maxx[2]
        mz = 179 / (maxz - minz)

        colors = (pts[:, 2] - minz) * mz

        values, counts = np.unique(colors, return_counts=True)

        # v = values[values < 25]
        # c = counts[values < 25]

        # print(np.sum(colors > 160))
        # print(colors)
        # plt.plot(values, counts)
        # plt.show()

        if np.sum(colors > 160) < 10000:
            pts = pts[colors < 160]
        elif np.sum(colors < 25) < 1000:
            pts = pts[colors > 25]
        else:
            break

    image = np.full((int(maxx[1]) + 1, int(maxx[0]) + 1, 3), 255, dtype=np.uint8)
    image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

    for point in pts:
        h = int((point[2] - minz) * mz)
        image[int(point[1]), int(point[0])] = [h, 255, 255]

    image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
    image = cv2.flip(image, 0)
    return image, minx_f, miny_f, minz, maxz


def pcd_to_jpg(path: str, fn: Union[str, bytes, os.PathLike]):
    h, points = read_pcd(fn)
    points_without_outlines = delete_outlines(points)
    image, minx, miny, minz, maxz = create_opencv_image(points_without_outlines)
    with open(os.path.join(path, "projection.txt"), 'w') as f:
        f.write(str(minx) + ' ' + str(miny) + ' ' + str(minz) + ' ' + str(maxz))
    cv2.imwrite(os.path.join(path, "projection.jpg"), image)
