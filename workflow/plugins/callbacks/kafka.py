import logging
import traceback
import json
import os

from confluent_kafka import Producer as KafkaProducer, KafkaError

from airflow.models.dag import Context, TaskInstance
from utils import get_conf_dict, get_resource_id, current_time, get_dag_payload_path
from callbacks.base import Callback

logger = logging.getLogger("airflow.task")


class KafkaCallback(Callback):
    def __init__(self, topics, producer_params):
        self.topics = topics
        self.producer_params = producer_params
        
        self.producer = None

    def run(self, context: Context):
        self.producer = KafkaProducer(**self.producer_params)
        messages = self.generate_messages(context)
        for message in messages:
            self.send_kafka_message(message)

    def generate_messages(self, context: Context):
        raise NotImplementedError

    def send_kafka_message(self, data):
        self.producer.poll(0)
        self.producer.produce(self.topics,
                              json.dumps(data).encode('utf-8'),
                              callback=self.kafka_delivery_report)
        self.producer.flush()

    @staticmethod
    def kafka_delivery_report(err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            logger.error('Kafka message delivery failed: {}'.format(err))
        else:
            logger.info('Kafka message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


class KafkaSuccessCallback(KafkaCallback):
    def generate_messages(self, context):
        path = os.path.join("/opt/airflow/payloads", get_resource_id(context))
        response = json.loads(open(os.path.join(path, "response.json")).read())
        return [{"ok": True, "result": response}]


class KafkaFailureCallback(KafkaCallback):
    def generate_messages(self, context):
        return [{"ok": False}]

