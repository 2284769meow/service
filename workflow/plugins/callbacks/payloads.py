from callbacks.base import Callback
from airflow.models.dag import Context
from utils.fs import get_dag_payload_path
import shutil


class ClearPayloadCallback(Callback):
    def run(self, context: Context):
        payload_path = get_dag_payload_path(context)
        shutil.rmtree(payload_path)