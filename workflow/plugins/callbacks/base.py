import logging
from collections.abc import Callable
from typing import List

from airflow.models.dag import Context

logger = logging.getLogger("airflow.task")


class Callback(Callable):
    def __call__(self, context: Context):
        logger.info(f"Callback {self.get_name()} starts")
        try:
            self.run(context)
        except Exception:
            logger.exception(f"Exception during {self.get_name()}")
        else:
            logger.info(f"Callback {self.get_name()} ends")

    def get_name(self):
        return self.__class__.__name__

    def run(self, context: Context):
        raise NotImplementedError


class CallbackList(Callable):
    def __init__(self, callback_list: List[Callback]):
        self.callback_list = callback_list

    def __call__(self, context: Context):
        for callback in self.callback_list:
            callback(context)
