# from pydantic import BaseModel
#
# from typing import Optional, Dict, List
#
#
# class BaseParams(BaseModel):
#     name: str
#     params: Optional[dict] = {}
#
#
# class PreprocessorParams(BaseModel):
#     datatype: str
#     transform: Optional[List[BaseParams]] = None
#
#
# class PostprocessorParams(BaseModel):
#     binary_data: bool = True
#     class_count: int = 0
#     transform: Optional[List[BaseParams]] = None
#
#
# class ConnectorParams(BaseModel):
#     url: str
#     verbose: bool = False
#     concurrency: int = 1
#     connection_timeout: float = 60.0
#     network_timeout: float = 60.0
#     ssl: bool = False
#     insecure: bool = False
#     max_greenlets: Optional[int] = None
#     ssl_options: Optional[str] = None
#     ssl_context_factory: Optional[str] = None
#
#
# class TritonImageClientStructure(BaseModel):
#     model_name: str
#     model_version: str
#     connector_params: ConnectorParams
#     preprocessors: Dict[str, PreprocessorParams]
#     postprocessors: Dict[str, PostprocessorParams]
