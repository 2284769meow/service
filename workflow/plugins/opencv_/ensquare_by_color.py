import numpy as np
import colorsys
import cv2
import os


def ensquare_by_color(image, color_lower, color_upper):
    color_lower = np.array(color_lower, np.uint8)
    color_upper = np.array(color_upper, np.uint8)

    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    color_mask = cv2.inRange(hsv_image, color_lower, color_upper)

    kernal = np.ones((5, 5), "uint8")

    color_mask = cv2.dilate(color_mask, kernal)

    # Creating contour to track color
    contours, hierarchy = cv2.findContours(color_mask,
                                           cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)

    coords = []
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        peri = cv2.arcLength(contour, True)

        circularity = peri/area

        if (area < 17000 and area > 500 and circularity < 0.1):
            x, y, w, h = cv2.boundingRect(contour)
            coords.append({"xmin": x, "ymin": y, "xmax": x + w, "ymax": y + h})

    return coords
