import logging
import os

import yaml
from airflow.decorators import dag, task
from airflow.utils.dates import days_ago

from utils import get_task_payloads_path, get_conf_dict, get_unique_dag_id, \
    get_current_dir_path, get_dag_payload_path
from callbacks import CallbackList, KafkaSuccessCallback, KafkaFailureCallback, ClearPayloadCallback

from opencv_.projection_recolor import pcd_to_jpg
from opencv_.rotate import rotate, proem
from opencv_.get_coords import *
from utils.context import get_resource_id
from opencv_.final_prediction import find_hole, check_hole, check_center
import requests
import json
import cv2

DAG_PATH = get_current_dir_path(__file__)

CONFIG_PATH = os.path.join("/configs", os.environ["CONFIG_NAME"])
CONFIG = yaml.load(open(CONFIG_PATH, "r"), Loader=yaml.FullLoader)

YOLO_CONFIG = CONFIG["yolo-server"]

CALLBACKS = CONFIG["callbacks"]
KAFKA_PARAMS = CALLBACKS["kafka"]
KAFKA = KAFKA_PARAMS["result"]

PROEM_DETECTOR = CONFIG["proem_detector"]

default_args = {
    'owner': 'airflow',
    'on_failure_callback': CallbackList(
        [
            KafkaFailureCallback(**KAFKA),
            # ClearPayloadCallback(),
        ]
    )
}

logger = logging.getLogger("airflow.task")


@dag(dag_id=get_unique_dag_id(__file__), default_args=default_args, schedule_interval=None, start_date=days_ago(2))
def yolo_safety_doors():
    @task()
    def converter():
        pcd_file_path = os.path.join(get_dag_payload_path(), "pointcloud.pcd")
        path = os.path.join("/opt/airflow/payloads", get_resource_id())
        os.mkdir(path)
        pcd_to_jpg(path, pcd_file_path)

    @task()
    def opencv_preprocessing() -> int:
        path = os.path.join("/opt/airflow/payloads", get_resource_id())
        im = cv2.imread(os.path.join(path, "projection.jpg"))
        rotated, start = rotate(im)
        cv2.imwrite(os.path.join(path, "rotated.jpg"), rotated)
        return int(start)

    @task()
    def yolo_detection():
        path = os.path.join("/opt/airflow/payloads", get_resource_id())
        projection_path = os.path.join(path, "projection.jpg")
        url = f"http://{YOLO_CONFIG['host']}:{YOLO_CONFIG['port']}/v1/object-detection/yolov5"
        files = {"image": open(projection_path, "rb")}
        response = requests.post(url, files=files)
        print(response.json())
        return response.json()

    @task()
    def door_detection(start_border: int) -> bool:
        path = os.path.join("/opt/airflow/payloads", get_resource_id())
        rotated = cv2.imread(os.path.join(path, "rotated.jpg"))
        detected_result = proem(rotated[start_border:])
        return bool(detected_result < int(PROEM_DETECTOR["offset"]))

    @task()
    def parse_yolo_results(opened: bool, yolo_results):
        path = os.path.join("/opt/airflow/payloads", get_resource_id())
        projection_txt_file = os.path.join(path, "projection.txt")
        projection_img_file = os.path.join(path, "projection.jpg")
        coords = get_info_from_file(projection_txt_file)
        im = cv2.imread(projection_img_file)
        figures = [generate_figure(yres['name'], *transform_coords(
            get_coords(yres['name'], im, ((int(yres['xmin']), int(yres['ymin'])), (int(yres['xmax']), int(yres['ymax']))), *coords)))
                   for yres in
                   yolo_results]
        return generate_json(figures, "opened" if opened else "closed")

    @task(on_success_callback=CallbackList([KafkaSuccessCallback(**KAFKA),]))
    def make_decision(result):
        path = os.path.join("/opt/airflow/payloads", get_resource_id())
        image = cv2.imread(os.path.join(path, "rotated.jpg"))
        s, e = find_hole(image)
        can = check_hole(image[s:e]) and check_center(image, (s + e) // 2)
        response = {
            "id": get_resource_id(),
            "result": json.loads(result),
            "safe": can,
        }
        with open(os.path.join(path, "response.json"), "w") as resp:
            resp.write(json.dumps(response))

    conv = converter()
    border_start = opencv_preprocessing()
    opened = door_detection(border_start)
    yolo_results = yolo_detection()
    parsed_yolo = parse_yolo_results(opened, yolo_results)

    conv >> border_start >> [opened, yolo_results] >> parsed_yolo
    make_decision(parsed_yolo)


safetydoors_yolo = yolo_safety_doors()
