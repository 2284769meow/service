import requests
import sys

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: request.py [url] [path to .pcd file]")
        sys.exit(0)
    url = sys.argv[1]
    point_cloud_file_path = sys.argv[2]
    point_cloud_file = {
        'file': open(point_cloud_file_path, 'rb')
    }
    data = {}
    resp = requests.post(url, files=point_cloud_file, data=data)
    print(resp.text)
